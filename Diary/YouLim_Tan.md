

This diary file is written by Tan You Lim E14065139 in the course Professional skills for engineering the third industrial revolution.

# 2019-02-20 #

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* I don't think exponential growth applies to food production.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I had seen Steven Pinker's TED talk.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.

# 2020-09-17 #

* I am happy I have the chance to give my presentation in the second week.
* I have learn alot about the conflit behaviour that professor disuss in the class.
* After watching Steven Pinker's TED Talk,I learn alot about the trending of the world nowadays.
* Professor said that the oil usage will be decreased beause of Covid-19 and the popularity of electric car.

# 2020-09-24 #

* In this week ,we learn alot about some mistakes that we make in our presentation ,and professor had given us many suggestions to help us to improve our presentations in the future.
* I have learned that how to detect fake news in the video of TED talk that professor shown us and nowadays media often uses click-bait or false news to cover the real situation.
* I also notice that the importane of copyright when preparing our presentation.We should always put the reference link in our presentation's material that is not from our idea or property.
* Lastly , professor taught us some ways to prove that the news is whether true or fake by using 'WHEN, WHERE, HOW, WHO, WHY and WHAT ' these six words can help us to find out the honesty of the news.

# 2020-10-15 #
* Today lesson i learned how the banking systems operates
* For the borrower and lender part, it states that when the borrower lends money from the lender, to maintain the liability for the borrower, debt is created to ensure that the lender will get his or her money back
* From the video teacher shown us about how our belief in fictional stories,like nations, money, and religion, shape society and have become so powerful that they shape our planet
* Lastly, our presentations are not very good because of not having unit on the x-axis and y-axis, and this is hard for the readers to understand our presentations

# 2020-10-22 #
* In today lessons, I learned about that most of the myths or fiction stories are so close and very related to our daily life.
* Teaher also explain that why the real estate in nowadays becomes more and more expensive and causes us cannot afford to buy  a house, and teaher states that this is because comercial banks are using the loans to gain profits
* Teaher also reminds us about "Thank you for your listening" should change to " Thank you for listening".
* In todays videos, I agree with having a healthcare system in our society instead of sickcacre system nowadays.
* Teaher has taught us some ways to find out how to make a testable hypothesis for our claims .

# 2020-10-29 #
* I was absent because i feel sick that day , maybe causes by the temperature changes.
* In the video i know that depression is very dangerous and we must care about our family and friends.
* Depression is not something we need to learn about it but also need to know how to prevent it like keeping ourselves in a good situation and relax our mind when feeling stress.
